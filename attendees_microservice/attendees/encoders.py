from common.json import ModelEncoder
from .models import ConferenceVO
from attendees.models import Attendee, AccountVO


class ConferenceVOEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {"conference": ConferenceVOEncoder()}

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email).count()
        if count > 0:
            return {"has_account": True}
        else:
            return {"has_accoutn": False}


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]
