import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
from django.core.mail import send_mail
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

# while True:
#     try:
#         def process_approval(ch, method, properties, body):
#             content = json.loads(body)
#             return send_mail(
#                 "Your presentation has been accepted",
#                 f"{content["presenter_name"]}, we're happy to inform you that your presentation {content["title"]} has been approved.",
#                 content["presenter_email"],
#                 "admin@conferencego.com",
#                 fail_silently=False,
#             )


#         def process_reject(ch, method, properties, body):
#             content = json.loads(body)
#             return send_mail(
#                 "Your presentation has been rejected",
#                 f"{content["presenter_name"]}, we're sorry to inform you that your presentation {content["title"]} has been rejected.",
#                 content["presenter_email"],
#                 "admin@conferencego.com",
#                 fail_silently=False,
#             )


#         def process_message(ch, method, properties, body):
#             print("Received %r" % body)


#         parameters = pika.ConnectionParameters(host="rabbitmq")
#         connection = pika.BlockingConnection(parameters)
#         channel = connection.channel()
#         channel.queue_declare(queue="presentation_approvals")
#         channel.basic_consume(
#             queue="presentation_approvals",
#             on_message_callback=process_message,
#             auto_ack=True,
#         )
#         channel.start_consuming()
#     except AMQPConnectionError:
#         print("Unable to connect")
#         time.sleep(2.0)


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        def process_rejection(ch, method, properties, body):
            presentation = json.loads(body)
            send_mail(
                "Presentation Rejected",
                message="We are regretful to inform you that your presentation has been rejected.",
                from_email="admin@conferencego.com",
                recipient_lis=[presentation["presenter_email"]],
                fail_silently=False,
            )

        def process_approval(ch, method, properties, body):
            presentation = json.loads(body)
            send_mail(
                "Presentation Approved",
                message="We are happy to inform you that your presentation has been approved!",
                from_email="admin@conferencego.com",
                recipient_list=[presentation["presenter_email"]],
                fail_silently=False,
            )

        channel.queue_declare(queue="presentation_rejection")
        channel.basic_consume(
            queue="presentation_rejection",
            on_message_callback=process_rejection,
            auto_ack=True,
        )

        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.start_consuming()

    except AMQPConnectionError:
        print("Unable to connect")
        time.sleep(2.0)
