import pika
import json


def send_presentation_to_queue(presentation, queue_name):  # Sends message to queueu
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue_name)

    presentation_dict = {
        "presenter_name": presentation.presenter_name,
        'presenter_email': presentation.presenter_email,
        "title": presentation.title
    }
    body = json.dumps(presentation_dict)

    channel.basic_publish(
        exchange="",
        routing_key=queue_name,
        body=body
    )
    connection.close()


def send_presentation_approval(presentation):
    send_presentation_to_queue(presentation, "presentation_approvals")


def send_presentation_rejection(presentation):
    send_presentation_to_queue(presentation, "presentation_rejection")
