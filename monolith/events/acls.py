from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

# Pexel Documentation https://www.pexels.com/api/documentation/
import requests

# Request documentation
# https://requests.readthedocs.io/en/latest/user/quickstart/#


def get_photo(city, state):
    header = {"Authorization": PEXELS_API_KEY}
    params = {"query": f"{city}, {state}", "per_page": 1}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=header, params=params)
    content = response.json()
    return {"picture_url": content["photos"][0]["src"]["original"]}


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {"q": f"{city},{state},US", "limit": 1, "appid": OPEN_WEATHER_API_KEY}
    response = requests.get(url, params=params)
    content = response.json()
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except (KeyError, IndexError):
        return None
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {"lat": lat, "lon": lon, "appid": OPEN_WEATHER_API_KEY, "units": "imperial"}
    response = requests.get(url, params=params)
    content = response.json()
    return {
        "description": content["weather"][0]["description"],
        "temp": content["main"]["temp"]
        }
