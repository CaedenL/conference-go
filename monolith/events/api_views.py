from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .models import Conference, Location, State
from .encoders import (
    ConferenceDetailEncoder,
    ConferenceListEncoder,
    LocationDetailEncoder,
    LocationListEncoder,
)
from .acls import get_photo, get_weather_data


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            conferences, encoder=ConferenceListEncoder, safe=False
        )
    else:  # POST
        content = json.loads(request.body)
    try:
        location = Location.objects.get(id=content["location"])
        content["location"] = location
    except Location.DoesNotExist:
        return JsonResponse({"message": "Invalid Location id"})
    conference = Conference.objects.create(**content)
    return JsonResponse(conference, encoder=ConferenceListEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        weather = get_weather_data(
            conference.location.city,
            conference.location.state.abbreviation
        )
        return JsonResponse(
            {"conference": conference, "weather": weather},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:  # PUT
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid Location id"}, status=400)
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference, encoder=ConferenceDetailEncoder, safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        location = Location.objects.all()
        return JsonResponse(
            location,
            encoder=LocationListEncoder,
            safe=False,
        )
    else:  # PUT or Create
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid State abbreviation"}, status=400
            )
        photo = get_photo(content["city"], content["state"])

        location = Location.objects.create(**content)
        return JsonResponse(
            {"location": location, "picture_url": photo},
            encoder=LocationListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        weather = get_weather_data(location.city, location.state.abbreviation)
        return JsonResponse(
            {"location": location, "weather": weather},
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid State abbreviation"}, status=400
            )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
