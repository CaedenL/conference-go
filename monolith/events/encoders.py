from common.json import ModelEncoder
from .models import Conference, Location, State
from presentations.models import Presentation, Status


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class StateListEncoder(ModelEncoder):
    model = State
    properties = ["name", "abbreviation"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "state",
        "picture_url",
    ]
    encoders = {"state": StateListEncoder()}


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {"location": LocationListEncoder()}


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


# class AttendeeDetailEncoder(ModelEncoder):
#     model = Attendee
#     properties = [
#         "email",
#         "name",
#         "company_name",
#         "created",
#         "conference",
#     ]
#     encoders = {"conference": ConferenceListEncoder()}


# class AttendeeListEncoder(ModelEncoder):
#     model = Attendee
#     properties = ["name"]


class StatusEncoder(ModelEncoder):
    model = Status
    properties = ["name"]


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title", "status"]
    encoders = {"status": StatusEncoder()}


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}
